# zammad-addon-form-widget-patches

form-widget-patches to zammad that we need while we wait for them to be upstreamed:

* [Consistently handle the 'disabled' attr in inputs #2929](https://github.com/zammad/zammad/pull/2929)
* [Fix deserializtion of multiselect value #2932](https://github.com/zammad/zammad/pull/2932)

## Development


### Create your addon
1. Edit the files in `src/`

   Migration files should go in `src/db/addon/form-widget-patches` ([see this post](https://community.zammad.org/t/automating-creation-of-custom-object-attributes/3831/2?u=abelxluck))

2. Update version and changelog in `form-widget-patches-skeleton.szpm`
3. Build a new package `make`

   This outputs `dist/form-widget-patches-vXXX.szpm`

4. Install the szpm using the zammad package manager.

5. Repeat


### Create a new migration

Included is a helper script to create new migrations. You must have the python
`inflection` library installed.

* debian/ubuntu:  `apt install python3-inflection`
* pip: `pip install --user inflection`
* or create your own venv

To make a new migration simply run:
```
make new-migration
```

## License

[![License GNU AGPL v3.0](https://img.shields.io/badge/License-AGPL%203.0-lightgrey.svg)](https://gitlab.com/digiresilience/link/zamamd-addon-form-widget-patches/blob/master/LICENSE.md)

This is a free software project licensed under the GNU Affero General
Public License v3.0 (GNU AGPLv3) by [The Center for Digital
Resilience](https://digiresilience.org) and [Guardian
Project](https://guardianproject.info).


🤠
