.PHONY: prep clean test build fmt new-migration

build: prep
	@./package.py
	@find dist/ -iname "*szpm"

prep:
	@mkdir -p dist

clean: prep
	@rm -rf dist/*

fmt:
	rufo src

new-migration:
	@./new-migration.py

test: build
